import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Cop } from './interfaces/cop';

@Injectable({
  providedIn: 'root'
})
export class SuspectsService {

  private URL = "http://ec2-3-223-129-137.compute-1.amazonaws.com";
  private recognition = "comp_face";
  private upload = "upload_image";


  constructor(private http:HttpClient) { }


  fileUpload(img: FormData):Observable<any> {
    return this.http.post(`${this.URL}/${this.upload}`,img);
    }

    comparePhotos(image: string):Observable<any>{
      return this.http.get<Cop>(`${this.URL}/${this.recognition}/suspect2.jpg/${image}`);
    }

}
