import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Cop } from './interfaces/cop';

@Injectable({
  providedIn: 'root'
})
export class CopsService {

  constructor(private http:HttpClient) { }

  private copsApi = 'http://ec2-3-223-129-137.compute-1.amazonaws.com';
  private getcops = "get_cops";
  private setcop = "set_cop";
  private deletecop ="del_cop";

  public getCops():Observable<Cop> {
    console.log("in the service")
      return this.http.get<Cop>(`${this.copsApi}/${this.getcops}`).pipe(
      map(res =>{
      console.log('in the service 2');
      console.log(res);
      return res;
    }));
    }

  public addCop(id:string,name:string,age:number,phone:string, rank:number):Observable<Cop>{
      console.log(id,name,age,phone,rank)
      console.log('jfjfjfjff',(`${this.copsApi}/${this.setcop}?id=${id}&name=${name}&age=${age}&phone=${phone}&rank=${rank}`));
      return this.http.get<Cop>(`${this.copsApi}/${this.setcop}?id=${id}&name=${name}&age=${age}&phone=${phone}&rank=${rank}`);
    }  

    public deleteCop(id:string):Observable<Cop>{
      console.log(id);
      console.log('ggg',(`${this.copsApi}/${this.deletecop}?id=${id}`))
      return this.http.get<Cop>(`${this.copsApi}/${this.deletecop}?id=${id}`);
    }

  
}


