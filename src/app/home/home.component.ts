import {CasesService} from './../cases.service';
import {AuthService} from './../auth.service';
import {Component, OnInit} from '@angular/core';
import {Case} from '../interfaces/case';
import {MatDialog} from '@angular/material/dialog';
import {CaseFormComponent} from '../case-form/case-form.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
}
