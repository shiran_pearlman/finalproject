import { TestBed } from '@angular/core/testing';

import { CopsService } from './cops.service';

describe('CopsService', () => {
  let service: CopsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CopsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
