import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { SuspectsService } from '../suspects.service';

@Component({
  selector: 'app-suspects',
  templateUrl: './suspects.component.html',
  styleUrls: ['./suspects.component.css']
})
export class SuspectsComponent implements OnInit {
  image:string ="shiran.jpeg";
  photo:string;
  path:string ="https://my-upload-image.s3.amazonaws.com/";
  match:boolean;
  nomatch:boolean;
  uploudedphoto: boolean;
  fileObj: File;
  imagename:string;
  similarity = null;
  userId = null;


  constructor(private suspectsService:SuspectsService, public authService:AuthService) { }


  compare(){
    console.log(this.imagename);
    return this.suspectsService.comparePhotos(this.imagename).subscribe(
      res => {
          this.similarity=res[0];
          console.log(this.similarity);
        }
    )
  }

  onFileUpload(event: Event): void{
    console.log(event);
    const FILE = (event.target as HTMLInputElement).files[0];
    this.fileObj = FILE;
    console.log(this.fileObj);
    
    const fileForm = new FormData();
    fileForm.append('img', this.fileObj);
    this.suspectsService.fileUpload(fileForm).subscribe(res => {
      console.log(res);
      this.imagename = res.imageName;
      this.photo = res.imageUrl;
      console.log(this.imagename);
      this.uploudedphoto = true;
      this.match = false;
      this.nomatch = false;
    });
  }



  ngOnInit(): void {
  }

}
