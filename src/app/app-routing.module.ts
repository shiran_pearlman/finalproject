import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CasesComponent} from './cases/cases.component';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from './login/login.component';
import {SignUpComponent} from './sign-up/sign-up.component';
import {NewsComponent} from './news/news.component';
import { CopsComponent } from './cops/cops.component';
import { CopFormComponent } from './cop-form/cop-form.component';
import { SuspectsComponent } from './suspects/suspects.component';


const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'news', component: NewsComponent},
  {path: 'cases', component: CasesComponent},
  {path: 'login', component: LoginComponent},
  {path: 'signup', component: SignUpComponent},
  {path: 'cops', component: CopsComponent},
  {path: 'addcop', component: CopFormComponent},
  {path: 'suspects', component: SuspectsComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
