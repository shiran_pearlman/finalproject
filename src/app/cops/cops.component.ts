import { Component, OnInit } from '@angular/core';
import { CopFormComponent } from '../cop-form/cop-form.component';
import { Cop } from '../interfaces/cop';
import { CopsService } from '../cops.service';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-cops',
  templateUrl: './cops.component.html',
  styleUrls: ['./cops.component.css']
})
export class CopsComponent implements OnInit {
  cops$:Observable<Cop>;
  cops:any;
  id:string;
  name:string;
  age:number;
  phone:string;
  rank:number;
  userId = null;


  navigateToCreateCop(){
    this.router.navigate(['/addcop']);
  }
  displayedColumns: string[] = ['id','name','age', 'phone', 'rank','Delete'];

  constructor(private copsService:CopsService, public authService:AuthService, private router:Router) { }

  ngOnInit(): void {
    console.log('1');
    this.copsService.getCops().subscribe(res =>{
      console.log('testtt',res)
      this.cops = res;
      console.log(this.cops);
              
    });

    
  }

  deleteCop(index){
    let id = this.cops[index].id;
    console.log('ddd',id); 
    this.copsService.deleteCop(id).subscribe(res =>{
      console.log(res)
      location.reload();
    });}


  

}
