import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { CopsService } from '../cops.service';
import { CopsComponent } from '../cops/cops.component';
import { Cop } from '../interfaces/cop';



@Component({
  selector: 'app-cop-form',
  templateUrl: './cop-form.component.html',
  styleUrls: ['./cop-form.component.css']
})
export class CopFormComponent implements OnInit {
  id:string;
  name:string;
  age:number;
  phone:string
  rank:number;

  onCancel(){
    this.router.navigate(['/cops']);
  }

  addCop(){
    this.copsService.addCop(this.id,this.name,this.age,this.phone, this.rank).subscribe(res =>{
      console.log(res)
    });
    console.log(this.id,this.name,this.age,this.phone, this.rank)
    this.router.navigate(['/cops']);
  }

  

  constructor(private authService:AuthService , private router:Router, private copsService:CopsService) { }

  ngOnInit(): void {

}
}


